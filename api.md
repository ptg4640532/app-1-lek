# Git Commands

- Git Init
 bash
$ git init 
$ git status


- Git Add, Commit
 bash
$ git add .
$ git add <file-name>
$ git commit -m "message"
$ git commit -m "message" -a

- Git Checkout
 bash
$ git checkout <hash>
$ git checkout <file-name> <hash>
$ git checkout <branch>
$ git checkout <branch> -b
$ git checkout <tag>

- Git bash
 bash
$ git checkout <branch>
