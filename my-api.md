# Git Commands

- Git Init
``` bash
$ git init 
$ git status
```


- Git Add, Commit
``` bash
$ git add .
$ git add <file-name>
$ git commit -m "message"
$ git commit -m "message" -a
```

- Git Clean and Reset
``` bash
$ git clean -n/f // Remove untracked files or folders
$ git reset --soft/--hard // Revert modified code to the checkout version
```


- Git Checkout
``` bash
$ git checkout <hash>
$ git checkout <file-name> <hash>
$ git checkout <branch>
$ git checkout <branch> -b
$ git checkout <tag>
```

- Git Branch
``` bash
$ git branch
$ git branch <new-branch-name>
$ git checkout <branch>
$ git branch -d/-D <branch-name>
```

- Git Tag
``` bash
$ git tag
$ git tag <new-tag-name>
$ git checkout <tag>
```

- Git Status
``` bash
$ git status
$ status [Untracked, Staged, Clean, Modified]
```

- Git Log
``` bash
$ git log 
$ git log --oneline
```

- Git Merge
``` bash
- All type of merges [fast-forward-merge, conflict-merge, rebase, squash, cherry-pick]

$ git merge <feature-branch>
$ git merge <feature-branch> --no-ff
$ git rebase <main-branch> // to update code from main branch
$ git rebase -i HEAD~2
$ git cherry-pick <commit-hash>
```

- Git Remote
``` bash 
- Remote: [origin,upstream]
$ git remote -v
$ git remote add origin/upstream <remote-address>
$ git remote remove origin/upstream
$ git remote set-url origin/upstream <remote-address>
```

- Git Push
``` bash 
$ git push 
$ git push origin <branch>
$ git push origin <tag> 
```

- Git Pull
``` bash 
$ git pull 
$ git push origin
$ git push origin <branch>
$ git push origin <tag> 
```

- Git Fecth
``` bash 
$ git Fecth 
$ git Fecth <branch>
$ git Fecth <tag> 
```